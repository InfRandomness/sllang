cmake_minimum_required(VERSION 3.15)

set(vcpkg_root "${CMAKE_SOURCE_DIR}/vcpkg/")
if (NOT EXISTS "${vcpkg_root}/.vcpkg-root")
    execute_process(COMMAND git submodule update --init "${vcpkg_root}")
endif ()
set(
        CMAKE_TOOLCHAIN_FILE "${vcpkg_root}/scripts/buildsystems/vcpkg.cmake"
        CACHE PATH "Path to vcpkg toolchain"
)

project(SLLang)

set(CMAKE_CXX_STANDARD 20)

add_executable(SLLang src/main.cxx)
